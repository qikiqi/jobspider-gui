#!/usr/bin/python3.4
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests
from urllib.parse import urljoin
import urllib.request, json 
import re
from os.path import expanduser
import collections
import calendar
import time
import pprint as pp
import codecs
from math import ceil


# try:
#     with reader(urllib.request.urlopen(startURL)) as jsondata:
#         jsondict = json.load(jsondata)
#         pp.pprint(jsondict)
# except urllib.error.HTTPError:
#     print("LOL")


## http://paikat.te-palvelut.fi/tpt-api/tyopaikat/9255500?kieli=fi
## FROM THIS? http://paikat.te-palvelut.fi/tpt-api/tyopaikat/9200000?kieli=fi
def molJson(url, reader):
    try:
        hitpage = urllib.request.urlopen(url)
        with reader(hitpage) as jsondata:
            return json.load(jsondata)
    except urllib.error.HTTPError:
        return False
    except urllib.error.URLError:
        return False

def saveOutputMolJson(json, jsontags):
    importantInfo = collections.OrderedDict()
    [importantInfo.update({str(x) : json["response"]["docs"][0][x]}) for x in json["response"]["docs"][0].keys() if x in jsontags]
    jobTitle = importantInfo["otsikko"]
    jobTitle = re.sub('[^0-9a-zA-ZåöäÅÖÄ]+', '_', jobTitle)
    jobTitle = re.sub('[åäÅÄ]+', 'a', jobTitle)
    jobTitle = re.sub('[öÖ]+', 'o', jobTitle)
    numOfJob = importantInfo["ilmoitusnumero"]
    try:
        preFormattedEndDate = importantInfo["hakuPaattyy"]
    except:
        preFormattedEndDate = "0000"
    formattedEndDate = preFormattedEndDate.split(".")
    formattedEndDate[-1] = formattedEndDate[-1][:4]
    formattedEndDate = ''.join(list(reversed(formattedEndDate)))
    jobFile = "/home/elementary/sync/oma/pytho/jobspiderQt/jobspider-gui/datatest/unclas/mol/" \
                + str(formattedEndDate) + "_" + jobTitle + "_" + str(numOfJob) + ".txt"
    with open(jobFile, "w") as f:
        f.write("%s\n\n" % jobTitle)
        for k, v in importantInfo.items():
            f.write("%s: %s\n" % (k, v))


def molJsonMaxPages(startNum, min, max):
    """ DEPRECATED, NOT IN USE """
    ## The idea here was to start from "safe" position and figure out the start and end of job postings
    ## The problem lies in the numbering, job postings are numbered ascending from the date that they were posted
    ## but some expire faster than others. So only solution is to scan individually every single page and see
    ## if it responds or not and then download it.
    reader = codecs.getreader("utf-8")
    maxfound, minfound = None, None
    minmaxlist = [min, max]
    trackerlist = [0, 1, 2]
    found = False
    for idx, minmaxbool in enumerate(minmaxlist):
        minus1, minus2 = (False, False) if idx == 0 else (True, True)
        increase = True if idx == 1 else False
        if minmaxbool is True:
            increment = 128 if idx == 1 else -128
            while found is False:
                startURL = "http://paikat.te-palvelut.fi/tpt-api/tyopaikat/" + str(startNum) + "?kieli=fi"
                try:
                    reader(urllib.request.urlopen(startURL),0.5)
                    print("FOUND")
                    minus2 = minus1
                    minus1 = True
                    if minus1 is not minus2:
                        increment = ceil(increment/2) if increase is True else -ceil(increment/2)
                    elif minus1 + minus2 == 0:
                        increment = -(increment)
                    trackerlist.pop(0)
                    trackerlist.append(startNum)
                    if trackerlist.count(trackerlist[0]) == len(trackerlist):
                        found = True
                except urllib.error.HTTPError:
                    print("ERROR")
                    minus2 = minus1
                    minus1 = False
                    if minus1 is not minus2:
                        increment = ceil(increment/2) if increase is True else -ceil(increment/2)
                    elif minus1 + minus2 == 2:
                        increment = -(increment)
                    trackerlist.pop(0)
                    trackerlist.append(startNum)
                    if trackerlist.count(trackerlist[0]) == len(trackerlist):
                        found = True
                startNum += increment
                print(startNum)
    #return maxfound, minfound


def molLight(startingURL, session):
    collectedUrls = []

    htmlPage = session.get(startingURL)
    soup = BeautifulSoup(htmlPage.content, 'html.parser')
    URLforRelative = "http://www.mol.fi/tyopaikat/tyopaikkatiedotus/kevyt/"

    # This finds all the jobs that molLightVersion has in one page
    for jobentry in soup.find_all("div", attrs={"class", "hakutulos"}):
        a = jobentry.findAll('a')[0]
        jobentryurl = a.attrs['href']
        jobURL = urljoin(URLforRelative, jobentryurl)
        jobURL = jobURL.split("&",1)[0]
        collectedUrls.append(jobURL)

    return collectedUrls

def molLightMaxPages(startURL, session):
    htmlPage = session.get(startURL)
    soup = BeautifulSoup(htmlPage.content, 'html.parser')
    print(soup)
    thisisit = None
    # This finds the maximum amount of pages to crawl through
    for jobentry in soup.find_all("div", attrs={"class", "hakutulosTitle"}):
        thisisit = re.search(r"/(\d*)", jobentry.text).group(1)
        try:
            thisisit = int(thisisit)
        except ValueError:
            return None
    return thisisit

def saveOutputMol(jobURL):
    jobPage = requests.get(jobURL)
    soup = BeautifulSoup(jobPage.content, 'html.parser')
    
    # TODO: Should this be parsed more or not?
    # Get the job description (THIS IS A POOR IMPLEMENTATION, because it relies too much on the index of the div. Problem is anonymoys div)
    breadIteration = 0
    for breadtext in soup.find_all("div"):
        if breadIteration == 2:
            jobDescriptionText = breadtext.text
            break
        breadIteration += 1

    # Get the job title and location for the filename
    for h3tag in soup.find_all("h3"):
        jobTitle = h3tag.text

    # Get key information about the job
    importantInfo = collections.OrderedDict()
    for importantDDs in soup.find_all("dd"):
        identifierDD = ''.join(re.findall(r'id=\"(.+?)\"', str(importantDDs)))
        importantInfo[identifierDD] = importantDDs.text
    
    jobTitle = re.sub('[^0-9a-zA-ZåöäÅÖÄ]+', '_', jobTitle)
    jobTitle = re.sub('[åäÅÄ]+', 'a', jobTitle)
    jobTitle = re.sub('[öÖ]+', 'o', jobTitle)
    numOfJob = importantInfo["ilmoitusnumero"]
    try:
        preFormattedEndDate = importantInfo["hakuPaattyy"]
    except:
        preFormattedEndDate = "0000"
    formattedEndDate = preFormattedEndDate.split(".")
    formattedEndDate[-1] = formattedEndDate[-1][:4]
    formattedEndDate = ''.join(list(reversed(formattedEndDate)))
    jobFile = "/home/elementary/sync/oma/pytho/jobspiderQt/datatest/unclas/mol/" + formattedEndDate + "_" + jobTitle + "_" + numOfJob + ".txt"
    with open(jobFile, "w") as f:
        f.write("%s\n\n" % jobTitle)
        for k, v in importantInfo.items():
            f.write("%s: %s\n" % (k, v))
        f.write("\n")
        f.write(jobDescriptionText)

def duunitoriMaxPages(startURL):
    # Find what is the maximum amount of pages to crawl through
    # find div class 'pagination--page' and parse 'Sivu ? / ????' and return it
    htmlPage = requests.get(startURL)
    soup = BeautifulSoup(htmlPage.content, 'html.parser')
    maxpages = None
    for jobentry in soup.find_all("div", attrs={"class", "pagination--page"}):
        maxpages = re.search(r"/ (\d*)", jobentry.text).group(1)
        try:
            maxpages = int(maxpages)
        except ValueError:
            return None
    return maxpages

def duunitori(startingURL):
    collectedUrls = []

    htmlPage = requests.get(startingURL)
    # If this breaks its because .text changed to .content, should not tho
    soup = BeautifulSoup(htmlPage.content, 'html.parser')
    URLforRelative = "https://duunitori.fi"

    # This finds all the jobs that duunitori has in one page and adds the urls to a list
    for jobentry in soup.find_all("a", attrs={"href","class", "jobentry--item"}):
        jobentryurl = jobentry.get("href")
        jobURL = urljoin(URLforRelative, jobentryurl)
        collectedUrls.append(jobURL)

    return collectedUrls

def saveOutputDuunitori(jobURL):
    jobPage = requests.get(jobURL)
    soup = BeautifulSoup(jobPage.content, 'html.parser')

    # This catches jobTitle
    for h1title in soup.find_all("h1", attrs={"class", "main--header"}):
        jobTitle = h1title.text

    # This catches company and location
    for h2companyAndLoc in soup.find_all("h2", attrs={"class", "page--info"}):
        companyAndLocation = re.findall(r'\">(.+?)<\/', str(h2companyAndLoc))
    
    # Get the long description of job application
    for divDescription in soup.find_all("div", attrs={"class", "long--description"}):
        jobDescription = divDescription.text
    
    importantInfoOrderDict = collections.OrderedDict()
    importantKeyInfo = collections.OrderedDict()
    importantInfo = collections.OrderedDict()
    
    keyInfoSpan = soup.find_all("span", attrs={"class", "col-xs-12 col-xl-4 jobentry--info--item"})
    infoSpan = soup.find_all("span", attrs={"class", "col-xs-12 col-xl-8"})
    
    for i in range(0, len(keyInfoSpan)):
        importantInfoOrderDict[re.sub('[\t\n]', '', keyInfoSpan[i].text)] = re.sub('[\t\n]', '', infoSpan[i].text)
    
    jobTitle = re.sub('[^0-9a-zA-ZåöäÅÖÄ]+', '_', jobTitle)
    jobTitle = re.sub('[åäÅÄ]+', 'a', jobTitle)
    jobTitle = re.sub('[öÖ]+', 'o', jobTitle)
    
    strCompAndLoc = ""
    for index in range(0, len(companyAndLocation)):
        companyAndLocation[index] = re.sub('[^0-9a-zA-ZåöäÅÖÄ]+', '_', companyAndLocation[index])
        companyAndLocation[index] = re.sub('[åä]+', 'a', companyAndLocation[index])
        companyAndLocation[index] = re.sub('[ö]+', 'o', companyAndLocation[index])
        strCompAndLoc = strCompAndLoc + companyAndLocation[index] + "_"
    
    numOfJob = calendar.timegm(time.gmtime())
    
    preFormattedEndDate = importantInfoOrderDict["Haku päättyy:"]

    formattedEndDate = preFormattedEndDate.split(".")
    formattedEndDate[-1] = formattedEndDate[-1][:4]
    formattedEndDate = ''.join(list(reversed(formattedEndDate)))
    if formattedEndDate == "Tois":
        formattedEndDate = "0000"
    jobFile = "/home/elementary/sync/oma/pytho/jobspiderQt/jobspider-gui/datatest/unclas/dun/" + formattedEndDate + "_" + jobTitle + "_" + strCompAndLoc + str(numOfJob) + ".txt"
    
    with open(jobFile, "w") as f:
        f.write("%s\n" % jobTitle)
        for k, v in importantInfoOrderDict.items():
            f.write("%s %s\n" % (k, v))
        f.write("\n")
        f.write(jobDescription)

if __name__ == '__main__':
    print("RUnning main")
    #molJsonMaxPages(9150500, True, False)
    jsontags = [
            "otsikko",
            "tehtavanimi",
            "viimeinenHakupaivamaara",
            "hakuPaattyy",
            "kuvausteksti",
            "hakemusLahetetaan",
            "ilmoituspaivamaarateksti",
            "tyoAlkaaTeksti",
            "tyoaika",
            "tyokokemus",
            "tyonLuonne",
            "tyonantajanNimi",
            "yhteystiedot",
            "wwwTyonhakulomake",
            "tyonantajanWwwOsoite",
            "tyopaikanOsoite",
            "kunta",
            "maa",
            "maakunta",
            "postinumero",
            "postitoimipaikka",
            "ilmoitusnumero",
            "id"
            ]
    reader = codecs.getreader("utf-8")
    json = molJson('http://paikat.te-palvelut.fi/tpt-api/tyopaikat/9255500?kieli=fi', reader)
    saveOutputMolJson(json, jsontags)