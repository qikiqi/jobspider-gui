#!/usr/bin/python3.4
# -*- coding: utf-8 -*-

## TODO:
#   0. Add basic logic for tracking already crawled urls
#   1. Monster + oikotie + MOL (NEW SITE)
#   2. Separate spiderclass to spiderclass&communicator
#   3. Documentation (Doxygen)
import sys
import codecs
import re
import urllib
import pprint as pp
import subprocess
import requests
import random
import time
import os

from PyQt5 import QtCore, QtGui, QtWidgets
from jobspiderLayout import Ui_MainWindow
from threadClass import SpiderClass

class JobspiderMain(Ui_MainWindow):
    """class for main window"""

    def __init__(self, MainWindow):
        Ui_MainWindow.__init__(self)
        self.setupUi(MainWindow)

        ## Make the connections
        self.actionQuit.triggered.connect(QtWidgets.QApplication.quit)

        self.model = QtWidgets.QFileSystemModel()
        self.rootFolder = "/home/elementary/sync/oma/pytho/jobspiderQt/jobspider-gui/datatest/"
        self.model.setRootPath(self.rootFolder)
        self.treeNoMatch.setModel(self.model)
        self.treeNoMatch.setRootIndex(self.model.index(self.rootFolder))
        self.treeNoMatch.setColumnHidden(1, True)
        self.treeNoMatch.setColumnHidden(2, True)
        self.treeNoMatch.setColumnWidth(0, 250)
        self.treeNoMatch.setSortingEnabled(True)
        self.treeNoMatch.doubleClicked.connect(self.openTxtFile)

        ## First setup the "physical" elements
        # progressBar
        self.progressBar = QtWidgets.QProgressBar()
        self.progressBar.reset()
        self.progressBar.setMinimum(0)
        self.progressBar.setMaximum(100)
        self.progressBar.setTextVisible(True)
        self.progressBar.setValue(99)


        ## Initialize statusbar
        self.lblCount = QtWidgets.QLabel()

        ## Initialize MOL spider
        self.molspider = SpiderClass(1, "http://www.mol.fi/tyopaikat/tyopaikkatiedotus/kevyt/hae.htm?lang=fi&tarkempiHaku=true&hakusana=&alueetLista=Pirkanmaa&_alueetLista=1&alueetLista=Uusimaa&_alueetLista=1&alueetLista=Varsinais-Suomi&_alueetLista=1&valitutAmmattialat=&_valitutAmmattialat=1&ilmoitettuPvm=1&vuokrapaikka=---")
        self.molspider.getData.connect(self.testprint)
        self.molspider.getInfoData.connect(self.setNumOfFiles)

        ## Initialize DUUNITORI spider
        self.duunitorispider = SpiderClass(2, "https://duunitori.fi/tyopaikat/?haku=&alue=Pääkaupunkiseutu&sho=&order_by=date_posted&sivu=0")
        self.duunitorispider.getData.connect(self.testprint)
        self.duunitorispider.getInfoData.connect(self.setNumOfFiles)

        ## Initialize MOLJSON spider
        self.moljsonspider = SpiderClass(3, threshold=-1)
        self.moljsonspider.getData.connect(self.testprint)
        self.moljsonspider.getInfoData.connect(self.setNumOfFiles)
        
        ## Add widgets to the statusbar
        self.statusbar.addWidget(self.progressBar)
        self.statusbar.addWidget(self.lblCount)

        # Number of files label
        self.numberOfFiles = self.getNumberOfFiles()
        self.lblCount.setText("Number of files: %s" % self.numberOfFiles)
        
        self.pbStart.clicked.connect(self.startTest)

    def testprint(self, arri):
        print("This is the length RESULTarray:")
        print(len(arri))

    def openTxtFile(self, *arg):
        # print(self.model.filePath(self.treeNoMatch.selectedIndexes()[0]))
        # txtLocation = self.rootFolder + arg[0].data()
        txtLocation = self.model.filePath(self.treeNoMatch.selectedIndexes()[0])
        if not os.path.isdir(txtLocation):
            with open(txtLocation, 'r') as myfile:
                data = myfile.read()
            self.textBrowser.setPlainText(data)

    def setNumOfFiles(self, amount):
        self.numberOfFiles += amount
        self.lblCount.setText("Number of files: %s" % (self.numberOfFiles))

    def startTest(self):
        print("START")
        if self.moljsonspider.isRunning():
            print("terminating?")
            self.moljsonspider.terminate()
        #self.duunitorispider.start()
        #self.molspider.start()
        self.moljsonspider.start()

    def getNumberOfFiles(self):
        return sum([len(files) for r, d, files in os.walk(self.rootFolder)])

        
if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    prog = JobspiderMain(MainWindow)
    MainWindow.show()
    sys.exit(app.exec())

