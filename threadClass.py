#!/usr/bin/python3.4
# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtGui, QtWidgets
from spiders import molLight, molLightMaxPages, saveOutputMol, duunitoriMaxPages, \
                    duunitori, saveOutputDuunitori, molJson, saveOutputMolJson
import subprocess
import socket
import time
import speedtest
import concurrent.futures
import requests
import urllib.request
import codecs

class SpiderClass(QtCore.QThread):
    
    # Create signal to pass server data
    getData = QtCore.pyqtSignal(list)

    # Create signal to pass info data
    getInfoData = QtCore.pyqtSignal(int)

    def __init__(self, mode, starturl = "", parent=None, threshold=0):
        super(SpiderClass, self).__init__(parent)
        self.mode = mode
        self.endlist = []
        self.urls = []
        self.starturl = starturl
        self.threshold = threshold
        if mode == 3:
            self.stopfutures = False
            self.reader = codecs.getreader("utf-8")
            self.falsecounter = 0

    ## Reimplemented run
    #  workflow: initialize endlist --> figure out the range to crawl
    #  --> gather urls from that range --> save urls to txt --> DONE
    #
    # @param self pointer to self
    # @pre mode has to be set
    def run(self):
        self.endlist = []
        gather = self.gatherUrls()
        if gather:
            self.saveUrls()
        else:
            print("gather failed")

    ## Methdod to gather all urls from one page or gather the json data in the case of molJson
    #  Mode 1: Mol (Deprecated)
    #  Mode 2: Duunitori
    #  Mode 3: MolJson
    #
    # @param self pointer to self
    # @pre self.endlist has to exist
    def gatherUrls(self):
        if self.mode == 1:
            session = requests.session()
            session.get(self.starturl)
            session.get(self.starturl)
            maxpages = molLightMaxPages(self.starturl, session)
            if maxpages is None:
                raise ValueError("MAXPAGES IS NONE")
            self.urls = self.molInit(maxpages)
        elif self.mode == 2:
            maxpages = duunitoriMaxPages(self.starturl)
            if maxpages is None:
                raise ValueError("MAXPAGES IS NONE")
            self.urls = self.duunitoriInit(maxpages)
        elif self.mode == 3:
            scanfrom = 9200000
            scanto = 9900000
            self.urls = self.molJsonInit(9255500, 9258500)
            print(self.urls)
        # We can use a with statement to ensure threads are cleaned up promptly
        with concurrent.futures.ThreadPoolExecutor(max_workers=30 if self.mode == 3 else 14) as executor:
            if self.mode == 1:
                # Start the load operation and mark each future with its URL
                future_to_url = {executor.submit(molLight, url, session):
                                 url for url in self.urls}
            elif self.mode == 2:
                future_to_url = {executor.submit(duunitori, url):
                                 url for url in self.urls}
            elif self.mode == 3:
                future_to_url = {executor.submit(self.molJsonWrapper, url, self.reader):
                                 url for url in self.urls}
            for future in concurrent.futures.as_completed(future_to_url):
                url = future_to_url[future]
                try:
                    if self.mode == 3:
                        result = future.result()
                        if result:
                            self.endlist.extend(result)
                        else:
                            self.falsecounter += 1
                            if self.falsecounter == self.threshold:
                                self.stopfutures = True
                                executor.shutdown()
                                return False
                    else:
                        self.endlist.extend(future.result())
                except Exception as exc:
                    print([time.strftime("%x - %X"), repr(exc)])
        if self.mode == 1:
            print(["Went in with:", len(self.urls), "urls, which should result into",
                   len(self.urls) * 25, "pages"])
        elif self.mode == 2:
            print(["Went in with:", len(self.urls), "urls, which should result into",
                   len(self.urls) * 20, "pages"])
        print(["Len of endlist:", len(self.endlist)])
        return True
        # # Send the list of job postings urls via signal
        # self.sendData(self.endlist)
    
    ## Wrapper to detect if stopfutures flag has been set or not,
    #  used to exit from ThreadPoolExecutor
    #
    # @param self pointer to self
    # @param url url pointing to the json file
    # @param reader StreamReader for utf-8
    def molJsonWrapper(self, url, reader):
        if not self.stopfutures:
            return molJson(url, reader)
        else:
            return False

    ## Creates the list of the range that is supposed to be crawled
    #
    # @param self pointer to self
    # @param maxpages maximum amount of the pages to crawl
    def molInit(self, maxpages):
        urls = ["http://www.mol.fi/tyopaikat/tyopaikkatiedotus/kevyt/vaihdasivu.htm?sivu=%s" % (x) for x in range(0,maxpages)]
        return urls

    ## Creates the list of the range that is supposed to be crawled
    #
    # @param self pointer to self
    # @param maxpages maximum amount of the pages to crawl
    def duunitoriInit(self, maxpages):
        urls = ["https://duunitori.fi/tyopaikat/?haku=&alue=Pääkaupunkiseutu&sho=&order_by=date_posted&sivu=%s" % (x) for x in range(0,maxpages)]
        return urls
    
    ## Creates the list of the range that is supposed to be crawled
    #
    # @param self pointer to self
    # @param scanfrom start of the range
    # @param scanto end of the range
    def molJsonInit(self, scanfrom, scanto):
        prefix = "http://paikat.te-palvelut.fi/tpt-api/tyopaikat/"
        suffix = "?kieli=fi"
        self.jsontags = [
            "otsikko",
            "tehtavanimi",
            "viimeinenHakupaivamaara",
            "hakuPaattyy",
            "kuvausteksti",
            "hakemusLahetetaan",
            "ilmoituspaivamaarateksti",
            "tyoAlkaaTeksti",
            "tyoaika",
            "tyokokemus",
            "tyonLuonne",
            "tyonantajanNimi",
            "yhteystiedot",
            "wwwTyonhakulomake",
            "tyonantajanWwwOsoite",
            "tyopaikanOsoite",
            "kunta",
            "maa",
            "maakunta",
            "postinumero",
            "postitoimipaikka",
            "ilmoitusnumero",
            "id"
            ]
        return [prefix + str(x) + suffix for x in range(scanfrom, scanto)]
        
    ## Methdod to download/save content from self.endlist to txt
    #  Mode 1: Mol (Deprecated)
    #  Mode 2: Duunitori
    #  Mode 3: MolJson
    #
    # @param self pointer to self
    # @pre self.endlist has to exist
    def saveUrls(self):
        print("Started saveUrls")
        counter = 0
        # We can use a with statement to ensure threads are cleaned up promptly
        with concurrent.futures.ThreadPoolExecutor(max_workers=25) as executor:
            if self.mode == 1:
                # Start the load operations and mark each future with its URL
                future_to_url = {executor.submit(saveOutputMol, url):
                                 url for url in self.endlist}
            elif self.mode == 2:
                future_to_url = {executor.submit(saveOutputDuunitori, url):
                                 url for url in self.endlist}
            elif self.mode == 3:
                future_to_url = {executor.submit(saveOutputMolJson, json, self.jsontags):
                                 json for json in self.endlist}
            for future in concurrent.futures.as_completed(future_to_url):
                url = future_to_url[future]
                try:
                    counter += 1
                    if counter % 10 == 0:
                        self.sendInfoData(counter)
                        counter = 0
                    future.result()
                except Exception as exc:
                    print("Got excep")
    
    def sendData(self, array):
        self.getData.emit(array)

    def sendInfoData(self, counter):
        self.getInfoData.emit(counter)

    def setMode(self, mode, urls = "", threshold = 0):
        self.mode = mode
        self.urls = urls
        self.threshold = threshold
        if mode == 3:
            self.stopfutures = False
            self.reader = codecs.getreader("utf-8")
            self.falsecounter = 0