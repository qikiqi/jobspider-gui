# README - jobspider-gui

Spider that automatically crawls and downloads all job postings from certain providers. Still heavily 
WIP.

## Dependencies

* BeautifulSoup4
* PyQt5

## FEATURES

* Extracts and parses the necessary information from job posting (txt format)
* Creates filenames based on the last apply date

TODO:

* Classify interesting and not interesting job posts with some NLP-classifier 
(http://www.aclweb.org/anthology/W/W16/W16-1617.pdf)
* and the list continues on file...
	
## SETUP & USAGE

Clone the repo and run:

>	python3.4 /path/to/file

or

>	python /path/to/file

## NOTE

Please do not abuse the webpages. Sleep at least 2 seconds before saving output.

